# Git Instructions 😉 #
---
## Para criar e enviar pela primeira vez: ##

1. **git init**
2. **git add** '*file*'
3. **git commit -m** '*message commit*'
4. **git remote add origin** '*https://Hakamad4@bitbucket.org/help3r/helperios.git*'
5. **git push -u origin** master

## Pela segunda é só: ##

1. **git add** '*file*'
2. **git commit -m** '*message*'
3. **git push -origin** '*https://Hakamad4@bitbucket.org/help3r/helperios.git*'

## Para dar pull: ##
1. **git clone** '*https://Hakamad4@bitbucket.org/help3r/helperios.git*'
2. **git pull origin** '*https://Hakamad4@bitbucket.org/help3r/helperios.git*' '*sua_branchV1.9*'

---